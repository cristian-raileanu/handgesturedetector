﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace HandGestureDetector
{
    class DetectionModule
    {
        // static fileds
        static int nPoint = 4;
        static int kCurvatureRadius = 10;
        static int kCurvatureK = 20; // 30;
        static int fingertipMinDist = 20;
        public int minArea = 10000;
        public int minHoleArea = 1000;
        static double minThumbIndexRatio = 1.25;
        static int minTriangleArea = 0;
        public double centersMediumDistance = 0.55;
        static double roiScaleRatio = 3.5;
        // object fields
        VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
        Mat hierarchy = new Mat();// = new Matrix<int>();
        VectorOfInt hull = new VectorOfInt();
        Matrix<int> defectsMatrix = new Matrix<int>(100,1, 4);

        bool hasHole;

        class Colors
        {
            public static MCvScalar BLUE = new MCvScalar(255, 0, 0);
            public static MCvScalar GREEN = new MCvScalar(0, 255, 0);
            public static MCvScalar RED = new MCvScalar(0, 0, 255);
            public static MCvScalar YELLOW = new MCvScalar(0, 255, 255);
            public static MCvScalar CYAN = new MCvScalar(255, 255, 0);
        }

        public enum HandSign
        {
            NOTHING, CLOSED_HAND, POINTING, GUN, PINCH, OK, CLAW, OPEN_HAND, FINGERS_2, FINGERS_3, FINGERS_4, SIGN_O
        };

        static String[] translations = {"Nothing", "Closed", "Pointing", "Gun", "Pinch",
        "Ok", "Claw", "Open", "2 fingers", "3 fingers", "4 fingers", "O" };

        // methods
        //
        public static String translate(int sign)
        {
            return translations[sign];
        }

        public void setDetection(int min_hand_area, double center_medium_distance)
        {
            minArea = min_hand_area;
            centersMediumDistance = center_medium_distance;
        }

        //
        //      runDetection --------------------------------------------------------------------------------
        //
        public int runDetection(Mat binaryImage, ref Mat contoursImage, int time)
        {
            contoursImage = new Mat(binaryImage.Height, binaryImage.Width, DepthType.Cv8U, 3);
            VectorOfPoint handContour = contourExtraction(binaryImage , minArea);
            if (handContour == null)
                return 0;

            //maxCircles = listAdapter(maxInscribedCircle, contours, (DetectionGlobalParams.nPoint, binaryImage.shape))
            CircleF maxCircle = maxInscribedCircle(handContour, nPoint);

            // reduce the ROI
            binaryImage = regionsOfInterest(binaryImage, new CircleF[1] { maxCircle }, roiScaleRatio);
            handContour = contourExtraction(binaryImage, minArea);
            //Console.WriteLine("after contour extraction");
            if (handContour == null)
                return 0;

            Mat defects = null;
            CircleF minCircle = new CircleF();
            processContour(handContour, ref hull, ref defects, ref minCircle);
            //Console.WriteLine("after contour processing");
            FingerInfoResult fingerInfoResult =
            fingerInformation(handContour, hull, defects, minCircle, maxCircle, fingertipMinDist, minThumbIndexRatio);
            //Console.WriteLine("after finger information");

            CvInvoke.DrawContours(contoursImage, new VectorOfVectorOfPoint(handContour), 0, Colors.RED);
            Point centerMax = new Point((int)maxCircle.Center.X, (int)maxCircle.Center.Y);
            CvInvoke.Circle(contoursImage, centerMax, (int)maxCircle.Radius, Colors.GREEN);
            Point centerMin = new Point((int)minCircle.Center.X, (int)minCircle.Center.Y);
            CvInvoke.Circle(contoursImage, centerMin, (int)minCircle.Radius, Colors.BLUE);

            if (!fingerInfoResult.isNull)
            foreach (KCurvatureResult finger in fingerInfoResult.kCurvatureResults)
            {
                CvInvoke.Circle(contoursImage, finger.peakPoint, 3, Colors.YELLOW);
                CvInvoke.Circle(contoursImage, finger.middlePoint, 3, Colors.CYAN);
            }

            int sign = 
            simpleGestureDetection(maxCircle, minCircle, fingerInfoResult, centersMediumDistance);

            return sign;
        }

        //
        //      contourExtraction --------------------------------------------------------------------------------
        //
        VectorOfPoint contourExtraction(Mat binaryImage, int threshAreaValue)
        {
            CvInvoke.FindContours(binaryImage, contours, hierarchy, RetrType.Ccomp, // was Tree
                                    ChainApproxMethod.ChainApproxSimple);
            //   _, contours, hierarchy = cv2.findContours(binaryImage, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            //# contour threshhold

            if (contours.Size == 0)
                return null;

            double largestArea = 0;
            int largestIdx = 0;
            //chosenContours = []

            VectorOfPoint contour;
            for (int i = 0; i < contours.Size; i++)
            {
                contour = contours[i];

                double area = CvInvoke.ContourArea(contour, false);
                if (area > largestArea)
                {
                    largestIdx = i;
                    largestArea = area;
                }
            }

            Matrix<int> managedHierarchy = new Matrix<int>(hierarchy.Rows, hierarchy.Cols, hierarchy.NumberOfChannels);
            hierarchy.CopyTo(managedHierarchy);
            Matrix<int>[] vectors = managedHierarchy.Split();
            double largestHoleArea = 0;
            for (int next = vectors[2][0, largestIdx]; next != -1; next = vectors[0][0, next] )
            {
                double area = CvInvoke.ContourArea(contours[next], false);
                if (area > largestHoleArea)
                    largestHoleArea = area;
            }

            if (largestHoleArea > minHoleArea)
                hasHole = true;
            else
                hasHole = false;

            VectorOfPoint maxContour = new VectorOfPoint(contours[largestIdx].ToArray());

            if (largestArea > threshAreaValue)
                return maxContour;
            else
                return null;
        }

        //
        //
        //
        CircleF maxInscribedCircle(VectorOfPoint contour, int nPoint)
        {

            //int nPoint = parameters[0];

            Rectangle rectange = CvInvoke.BoundingRectangle(contour);
            int x, y, w, h;
            x = rectange.X;
            y = rectange.Y;
            w = rectange.Width;
            h = rectange.Height;

            w = w / 3;
            h = h / 3;
            x += w;
            y += h;

            int cx, cy, step = 3, minDist, i, contourLen = contour.Size , radius = 0, centerx = 0, centery=0;
            Point[] contourPoints = contour.ToArray();

            Point newCenter = new Point();
            PointF point = new PointF();
            double dist = 0;

            for (cx = x; cx < x + w; cx += step)
                for (cy = y; cy <= y + h; cy += step)
                {
                    minDist = 10 * 1000 * 1000;
                    newCenter.X = cx;
                    newCenter.Y = cy;// = new Point(cx, cy);
                    for (i = 0; i < contourLen; i += nPoint)
                    {
                        point = contourPoints[i];

                        dist = Math.Sqrt(Math.Pow(newCenter.X - point.X, 2) + Math.Pow(newCenter.Y - point.Y, 2));

                        if (dist < minDist)
                            minDist = (int)dist;
                    }
                    if (minDist > radius)
                    {
                        centerx = cx;
                        centery = cy;
                        radius = minDist;
                    }
                }

            CircleF circle = new CircleF();
            circle.Center = new PointF(centerx, centery);
            circle.Radius = radius;

            return circle;
        }

        //
        //      regionsOfInterest
        //
        Mat regionsOfInterest(Mat image, CircleF[] circles, double scaleNr)
        {
            Mat newImage = new Mat(image.Height, image.Width, DepthType.Cv8U, 1);
            //  newImage = np.zeros(image.shape, dtype = np.uint8)


            //  for circle in circles:
            foreach (CircleF circle in circles)
            {
                int x = (int)circle.Center.X, y = (int)circle.Center.Y;
                int radius = (int) (circle.Radius * scaleNr);
                int smallRadius = (int)circle.Radius;

                int x1 = Math.Max(1, x - radius);
                int y1 = Math.Max(1, y - radius);
                int x2 = Math.Min(image.Width , x + radius);
                int y2 = Math.Min(image.Height, y + smallRadius);

                Rectangle roiRectangle = new Rectangle(x1, y1, x2 - x1-1, y2 - y1-1);
                // copy pixels
                Mat piece = new Mat(image, roiRectangle);
                //Console.WriteLine("after piece");
                Mat newPiece = new Mat(newImage, roiRectangle);
                piece.CopyTo(newPiece);
                //Console.WriteLine("after copy");
            }
            

            return newImage;
        }

        //
        //      processContour
        //
        void processContour(VectorOfPoint contour, ref VectorOfInt hull, ref Mat defects, ref CircleF minCircle)
        {
            CvInvoke.ConvexHull(contour, hull, false, false);
            //Console.WriteLine("after convex hull");
            //hull = cv2.convexHull(contour, returnPoints = False)
            defects = new Mat();
            CvInvoke.ConvexityDefects(contour, hull, defects);
            //Console.WriteLine("after convexity defects");
            int rows = Math.Max(defects.Rows, 1);
            int cols = Math.Max(defects.Cols, 1);
            int nrchannels = Math.Max(defects.NumberOfChannels, 1);

            defectsMatrix = new Matrix<int>(rows, cols, nrchannels);
            defects.CopyTo(defectsMatrix);      // needs optimization
            //Console.WriteLine("after copy matrix");
            minCircle = CvInvoke.MinEnclosingCircle(contour);
        }

        //
        //
        //
        FingerInfoResult fingerInformation(VectorOfPoint contour, VectorOfInt hull, Mat defects, 
            CircleF minCircle, CircleF maxCircle, int fingertipMinDist, double minRatio)
        {
            
            int ra = (int)maxCircle.Radius;
            int rb = (int)minCircle.Radius;
            
            List<VectorOfInt> convexityDefects = new List<VectorOfInt>();
            List<int> Ap = new List<int>();
            int thumb = 0, index = 0, maxTriangleArea = minTriangleArea;
         
            if (defects.Rows == 0 )
            {
                return new FingerInfoResult(null, false, 0);
            }
            
            Matrix<int>[] channels = defectsMatrix.Split();
            int s, e, f, ld;

            bool hasThumb = false;
            int thumbIndexAngle = 0;

            for (int i= 0; i<defects.Rows; i++ )
            {
                //       s, e, f, ld = defects[i, 0]
                s = channels[0][i, 0];
                e = channels[1][i, 0];
                f = channels[2][i, 0];
                ld = channels[3][i, 0];

                //       pStart = tuple(contour[s][0])
                PointF pStart = contour.ToArray()[s];
                //       pEnd = tuple(contour[e][0])
                PointF pEnd = contour.ToArray()[e];
                //       pFar = tuple(contour[f][0])
                PointF pFar = contour.ToArray()[f];
                //       ld /= 256
                ld /= 256;

                //       if ld >= ra and ld <= rb and angle(pFar, pStart, pEnd) < 100:
                if (ld >= ra && ld <= rb)
                {
                    if (closestDist(contour, Ap, pStart) > fingertipMinDist)
                        Ap.Add(s);
                    
                    if (closestDist(contour, Ap, pEnd) > fingertipMinDist)
                        Ap.Add(e);

                    if (triangleArea(pStart, pEnd, pFar) > maxTriangleArea)
                    {
                        maxTriangleArea = triangleArea(pStart, pEnd, pFar);

                        int distFS = distance(pFar, pStart);
                        int distFE = distance(pFar, pEnd);
                        if (distFS < distFE)
                        {
                            if (distFE / distFS > minRatio)
                            {
                                //thumb = Ap.Count - 2;
                                //index = Ap.Count - 1;
                                hasThumb = true;
                                thumbIndexAngle = angle(pFar, pStart, pEnd);
                            }
                        }
                        else
                            if (distFS / distFE > minRatio)
                            {
                                //thumb = Ap.Count - 1;
                                //index = Ap.Count - 2;
                                hasThumb = true;
                                thumbIndexAngle = angle(pFar, pStart, pEnd);
                            }

                    }

                }

            }


            List<KCurvatureResult> kCurvatureResults = new List<KCurvatureResult>();
            for ( int i = 0; i < Ap.Count; i++)
            {
                int pos = Ap.ElementAt(i);
                KCurvatureResult result = kCurvature(pos, contour, kCurvatureRadius, kCurvatureK);
                if (result.minAngle < 70)
                {
                    // correct in the new list
                    //if (i == thumb)
                    //    thumb = kCurvatureResults.Count;
                    //if (i == index)
                    //    index = kCurvatureResults.Count;
                    kCurvatureResults.Add(result);
                }
            }
            
            return new FingerInfoResult(kCurvatureResults, hasThumb, thumbIndexAngle);
        }

        //
        //
        struct FingerInfoResult
        {
            public List<KCurvatureResult> kCurvatureResults;
            //public int thumb;
            //public int index;
            public int thumbIndexAngle;
            public Boolean isNull;
            public Boolean hasThumbIndex;

            public FingerInfoResult(List<KCurvatureResult> results, bool _hasThumbIndex, int angle )
            {
                kCurvatureResults = results;
                
                if (results == null)
                    isNull = true;
                else
                    isNull = false;
                hasThumbIndex = _hasThumbIndex;
                thumbIndexAngle = angle;
            }
        }


        //
        //      closestDist
        //      used in : fingerInformation
        int closestDist(VectorOfPoint contour, List<int> lst, PointF point)
        {
            int minDist = 100000;
            
            foreach (int ind in lst)
            {
                PointF currentPoint = contour.ToArray()[ind];

                int dist = distance(point, currentPoint);
                if (dist < minDist)
                    minDist = dist;
            }
                
            return minDist;
        }
        //
        //      triangleArea
        //      used in : fingerInformation
        int triangleArea(PointF a, PointF b, PointF c)
        {
            int l1 = distance(a, b);
            int l2 = distance(a, c);
            int l3 = distance(b, c);
            float s = (l1 + l2 + l3) / 2;
            return (int) Math.Sqrt(s * (s - l1) * (s - l2) * (s - l3));
        }

        int angle(PointF origin, PointF p1, PointF p2)
        {
            int[] p1Arr = { (int)(p1.X - origin.X), (int)(p1.Y - origin.Y) };
            int[] p2Arr = { (int)(p2.X - origin.X), (int)(p2.Y - origin.Y) };
            Matrix<int> v1 = new Matrix<int>( p1Arr);
            Matrix<int> v2 = new Matrix<int>(p2Arr);

            double cos = v1.DotProduct(v2) / (v1.Norm * v2.Norm);
            double teta = Math.Acos(cos) * 180 / Math.PI; // in degrees
            return (int) teta;
        }
            
        int distance(PointF p1, PointF p2)
        {
            return (int) Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        //
        //      kCurvature
        //
        KCurvatureResult kCurvature(int pos, VectorOfPoint contour, int radius, int k)
        {
            //   contour, radius, k = params

            //peakPoint, middlePoint, minAngle = (0, 0), (0, 0) , 180
            Point peakPoint = new Point(0, 0);
            Point middlePoint = new Point(0, 0);
            int minAngle = 180;

            int nrContourPoints = contour.Size;

            //for i in range(-radius, radius):
            Point origin, left, right;
            Point[] contourPoints = contour.ToArray();
            int originPos, leftPos, rightPos, teta;

            for (int i = -radius; i < radius; i++)
            {
                originPos = (pos + i + nrContourPoints) % nrContourPoints;
                leftPos = (originPos - k + nrContourPoints) % nrContourPoints;
                rightPos = (originPos + k + nrContourPoints) % nrContourPoints;

                origin = contourPoints[originPos];
                left = contourPoints[leftPos];
                right = contourPoints[rightPos];

                teta = angle(origin, left, right);

                if (teta < minAngle)
                {
                    minAngle = teta;

                    peakPoint = origin;
                    //middlePoint = (np.array(left) + np.array(right)) / 2;
                    middlePoint = new Point((left.X + right.X) / 2, (left.Y + right.Y) / 2);
                }
         
            }

            return new KCurvatureResult(peakPoint, middlePoint, minAngle);
        }
        
        struct KCurvatureResult
        {
            public int minAngle;
            public Point peakPoint;
            public Point middlePoint;

            public KCurvatureResult(Point peak, Point middle, int angle)
            {
                peakPoint = peak;
                middlePoint = middle;
                minAngle = angle;
            }
        }
            

        Point pointDif(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }


        //
        //
        //
        int simpleGestureDetection(CircleF maxCircle, CircleF minCircle, FingerInfoResult fingerInfo, double mediumDistance)
        {
            mediumDistance *= (int)maxCircle.Radius;

            int distInsEncl = distance(maxCircle.Center, minCircle.Center);

            //int thumbId = fingerInfo.thumb, indexId = fingerInfo.index;
            //Console.WriteLine("fingerInfo null? " + fingerInfo.isNull);
            if (fingerInfo.isNull)
                return 0;

            List<KCurvatureResult> fingers = fingerInfo.kCurvatureResults;

            int nrFingers = fingers.Count, angleTI = 0;
            angleTI = fingerInfo.thumbIndexAngle;
            //if (fingerInfo.hasThumbIndex)
            //{
            //    PointF vect1 = pointDif(fingers[thumbId].peakPoint, fingers[thumbId].middlePoint);
            //    PointF vect2 = pointDif(fingers[indexId].peakPoint, fingers[indexId].middlePoint);
            //    angleTI = angle(new PointF(0, 0), vect1, vect2);
            //    Console.WriteLine("angle= " + angleTI);
            //}

            int sign = 0;

            if (hasHole)
                if (nrFingers > 2)
                    return (int)HandSign.OK;
                else
                    return (int)HandSign.SIGN_O;

            if (nrFingers <= 1)
            {
                if (distInsEncl <= mediumDistance)
                    sign = (int)HandSign.CLOSED_HAND; // HandSigns.closedHand;
                else
                    sign = (int)HandSign.POINTING;
            }

            if (nrFingers == 2)
            {
                if (!fingerInfo.hasThumbIndex)
                    sign = (int)HandSign.FINGERS_2;
                else
                {
                    if (angleTI < 60)
                        sign = (int)HandSign.PINCH; //HandSigns.pinch
                    else
                        sign = (int)HandSign.GUN; //HandSigns.gun
                }
            }

            if (nrFingers == 3)
            {
                sign = (int)HandSign.FINGERS_3;
            }

            if (nrFingers >= 4)
            {
                if (distInsEncl <= mediumDistance)
                    sign = (int)HandSign.CLAW;
                else
                    sign = (int)HandSign.OPEN_HAND;
            }

            return sign;
        }

    }
}

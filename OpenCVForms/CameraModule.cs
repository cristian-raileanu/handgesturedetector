﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace HandGestureDetector
{
    class CameraModule
    {
        // fields
        static String backgroundFilePath = "background.png";
        static String haarCascadePath_old =
        "E:\\Programe\\opencv\\source\\data\\haarcascades\\haarcascade_frontalface_default.xml";
        static String haarCascadePath =
        "haarcascade_frontalface_default.xml";
        public byte[] yRangeSkin =  {10, 163};      //new ScalarArray(new MCvScalar(10, 163));
        public byte[] crRangeSkin = { 131, 157 };  //new ScalarArray(new MCvScalar(131, 157));
        public byte[] cbRangeSkin = { 110, 135 }; //new ScalarArray(new MCvScalar(110, 135));
        public byte[] yRangeBackground = { 50, 255 };
        public byte[] crRangeBackground = { 10, 150 };
        public byte[] cbRangeBackground = { 30, 250 };


        Rectangle[] lastRectangles = null;

        CascadeClassifier faceCascade = new CascadeClassifier(haarCascadePath);
        Mat background = new Mat(backgroundFilePath);
        // temporary
        Mat normalizedFrame = new Mat(480, 640, DepthType.Cv8U, 3);
        Mat extractionResult = new Mat(480, 640, DepthType.Cv8U, 3);
        
        //
        // methods ----------------------------------------
        //

        //
        //      runCamera()
        //
        public void runCamera(Mat frame, ref Mat _foreground, ref Mat _binarySkin )
        {
            
            Mat background = backgroundSubstraction(frame, backgroundFilePath);
            //background = backgroundSubstraction(normalizedFrame, GlobalParams.backgroundFilePath)
            
            Mat binaryForeground = processChannels(background);
            //binaryForeground = processChannels(background)

            //CvInvoke.Imshow("binaryForeground", binaryForeground);
            Mat foreground = new Mat();  // will be BGR
            CvInvoke.BitwiseAnd(frame, frame, foreground, binaryForeground);
            //foreground = cv2.bitwise_and(normalizedFrame, normalizedFrame, mask = binaryForeground)
            //CvInvoke.Imshow("foreground", foreground);

            Rectangle[] faces = faceRectangles(frame);
            //faces = faceRectangles(frame)
            if (faces == null || faces.Length == 0)
                faces = lastRectangles;
            else
                lastRectangles = faces;

            foreground = faceRemoval(foreground, faces);
            //foreground = faceRemoval(foreground, binaryForeground, faces)

            //# cannyEdge(foreground)

            Mat handsBinary = getSkin(foreground, yRangeSkin, crRangeSkin, cbRangeSkin);
            // handsBinary = getSkin(foreground, GlobalParams.yRange, GlobalParams.crRange, GlobalParams.cbRange)
            
            _foreground = foreground;
            _binarySkin = handsBinary;
        }

        //
        //      backgroundSubstraction()
        //
        Mat backgroundSubstraction(Mat frame, String backgroundFilePath)
        {
            
            CvInvoke.CvtColor(frame, normalizedFrame, ColorConversion.Bgr2YCrCb);
            
            CvInvoke.CvtColor(background, background, ColorConversion.Bgr2YCrCb);
            
            CvInvoke.AbsDiff(normalizedFrame, background, extractionResult);

            return extractionResult;
        }

        //
        //      faceRectangles()
        //
        Rectangle[] faceRectangles(Mat rgbImage)
        {
            Mat gray = new Mat(rgbImage.Height, rgbImage.Width, DepthType.Cv8U, 3);
            CvInvoke.CvtColor(rgbImage, gray, ColorConversion.Bgr2Gray);
            //gray = cv2.cvtColor(rgbImage, cv2.COLOR_BGR2GRAY)
            
            Rectangle [] faces = faceCascade.DetectMultiScale(gray, 1.3, 5);
            //faces = face_cascade.detectMultiScale(gray, 1.3, 5)

            return faces;
        }

        //
        //
        //
        Mat processChannels(Mat normalizeImage)
        {
            Mat[] channels = normalizeImage.Split();
            Mat channelY = processChannel(channels[0],yRangeBackground[0], yRangeBackground[1] );
            Mat channelCr = processChannel(channels[1], crRangeBackground[0], crRangeBackground[1] );
            Mat channelCb = processChannel(channels[2], cbRangeBackground[0], cbRangeBackground[1] );
            //   channelY = processChannel(normalizedImage[:,:, 0], 50, 255)
            //   channelCr = processChannel(normalizedImage[:,:, 1], 5, 50)
            //   channelCb = processChannel(normalizedImage[:,:, 2], 5, 250)
            //CvInvoke.Imshow("Channel", channelCr);
            
            Mat binary = channelCb;
            CvInvoke.BitwiseOr(channelCr, channelCb, binary);
            CvInvoke.BitwiseOr(binary, channelY, binary);

            //#kernel = np.ones((5, 5), np.uint8)
            //#kernel = np.ones((5, 5), np.uint8)
            //#mask = cv2.morphologyEx(binary, cv2.MORPH_CLOSE, kernel)
            //Mat kernel = new Mat(5, 5, DepthType.Cv8U, 1);
            //kernel.SetTo(new MCvScalar(1));
            //CvInvoke.MorphologyEx(binary, binary, MorphOp.Close, kernel,
            //                        new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1));

            return binary;
        }

        //
        //
        //
        Mat processChannel(Mat channel, int lowerValue, int upperValue)
        {
            Mat mask = channel;
            ScalarArray lowerArray = new ScalarArray(new MCvScalar(lowerValue));
            ScalarArray upperArray = new ScalarArray(new MCvScalar(upperValue));
            CvInvoke.InRange(channel, lowerArray, upperArray, mask );
            //   mask = cv2.inRange(channel, lowerValue, upperValue)

            //# morphological transformations

            Mat kernel = new Mat(5, 5, DepthType.Cv8U, 1);
            kernel.SetTo(new MCvScalar(1));
            //kernel = np.ones((5, 5), np.uint8)

            CvInvoke.MorphologyEx(mask, mask, MorphOp.Close, kernel, 
                                    new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1));
            //mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

            return mask;
        }

        //
        //      faceRemoval
        //
        Mat faceRemoval(Mat image, Rectangle[] faces)
        {
            if (faces == null)
                return image;

            for (int i = 0; i < faces.Length; i++)
            {
                int height = (int)(faces[i].Height * 1.5);
                height = Math.Min(height, image.Height - faces[i].Y -1);
                faces[i].Height = height;
            }

            foreach (Rectangle face in faces)
            {
                Mat faceRegion = new Mat(image, face );
                faceRegion.SetTo(new Bgr(0, 0, 0).MCvScalar);
                //image[y: y + int(h * 1.5), x: x + w, : ] = 0
            }
                        
            return image;
        }

        //
        //      getSkin
        //
        Mat getSkin(Mat frame, byte[] yRange, byte[] crRange, byte[] cbRange )
        {
            Mat normalized = new Mat();
            CvInvoke.CvtColor(frame, normalized, ColorConversion.Bgr2YCrCb);
            ScalarArray lowerSkin = new ScalarArray(new MCvScalar(yRange[0], crRange[0], cbRange[0] ));
            ScalarArray upperSkin = new ScalarArray(new MCvScalar(yRange[1], crRange[1], cbRange[1] ));
            //   lower_skin = np.array([yRange[0], crRange[0], cbRange[0]])
            //upper_skin = np.array([yRange[1], crRange[1], cbRange[1]])
            Mat mask = new Mat(normalized.Height, normalized.Width, DepthType.Cv8U, 1);
            CvInvoke.InRange(normalized, lowerSkin, upperSkin, mask);
            //mask = cv2.inRange(normalized, lower_skin, upper_skin)

            //   # morphological transformations
            //   kernel = np.ones((5, 5), np.uint8)

            Mat kernel = new Mat(5, 5, DepthType.Cv8U, 1);
            kernel.SetTo(new MCvScalar(1));
            CvInvoke.MorphologyEx(mask, mask, MorphOp.Close, kernel,
                                    new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1));
            //mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
            //   normalized = cv2.GaussianBlur(normalized, (5, 5), 1, 1)
            return mask;
        }

        //
        //
        //
        void cannyEdge(Mat image)
        {
            //# cv2.imshow('input image', image)
            Mat gray = new Mat(image.Height, image.Width, DepthType.Cv8U, 1);
            //gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            //edges = cv2.Canny(gray, 5, 20)
            //cv2.add(image, 255, dst = image, mask = edges)
            //# cv2.imshow('image_edges', edges)
        }


        public void setCamera(byte[] yBack, byte[] crBack, byte[] cbBack, byte[] ySkin, byte[] crSkin, byte[] cbSkin)
        {
            yRangeBackground = yBack;
            crRangeBackground = crBack;
            cbRangeBackground = cbBack;
            yRangeSkin = ySkin;
            crRangeSkin = crSkin;
            cbRangeSkin = cbSkin;
        }
            
    }
}

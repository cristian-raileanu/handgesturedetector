﻿namespace HandGestureDetector
{
    partial class SelectionRangeSliderValues
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_min = new System.Windows.Forms.Label();
            this.label_max = new System.Windows.Forms.Label();
            this.selectionRangeSlider1 = new HandGestureDetector.SelectionRangeSlider();
            this.SuspendLayout();
            // 
            // label_min
            // 
            this.label_min.AutoSize = true;
            this.label_min.BackColor = System.Drawing.Color.Transparent;
            this.label_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_min.Location = new System.Drawing.Point(6, -1);
            this.label_min.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label_min.Name = "label_min";
            this.label_min.Size = new System.Drawing.Size(36, 20);
            this.label_min.TabIndex = 1;
            this.label_min.Text = "255";
            this.label_min.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_max
            // 
            this.label_max.AutoSize = true;
            this.label_max.BackColor = System.Drawing.Color.Transparent;
            this.label_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_max.Location = new System.Drawing.Point(219, 0);
            this.label_max.Name = "label_max";
            this.label_max.Size = new System.Drawing.Size(36, 20);
            this.label_max.TabIndex = 2;
            this.label_max.Text = "255";
            this.label_max.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // selectionRangeSlider1
            // 
            this.selectionRangeSlider1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.selectionRangeSlider1.Location = new System.Drawing.Point(45, -1);
            this.selectionRangeSlider1.Max = 255;
            this.selectionRangeSlider1.Min = 0;
            this.selectionRangeSlider1.MinimumSize = new System.Drawing.Size(30, 20);
            this.selectionRangeSlider1.Name = "selectionRangeSlider1";
            this.selectionRangeSlider1.SelectedMax = 255;
            this.selectionRangeSlider1.SelectedMin = 0;
            this.selectionRangeSlider1.Size = new System.Drawing.Size(172, 21);
            this.selectionRangeSlider1.TabIndex = 0;
            this.selectionRangeSlider1.Value = 50;
            // 
            // SelectionRangeSliderValues
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_max);
            this.Controls.Add(this.label_min);
            this.Controls.Add(this.selectionRangeSlider1);
            this.Name = "SelectionRangeSliderValues";
            this.Size = new System.Drawing.Size(255, 23);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SelectionRangeSlider selectionRangeSlider1;
        private System.Windows.Forms.Label label_min;
        private System.Windows.Forms.Label label_max;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using System.Timers;
using System.IO;
using System.Diagnostics;

namespace HandGestureDetector
{
    public partial class MainWindowForm : Form
    {
        AboutBox1 aboutBox;
        OptionsForm optionsForm;
        UserHelp userHelpForm;

        VideoCapture videoCapture;
        Mat frame = new Mat();
        CameraModule cameraModule = new CameraModule();
        DetectionModule detectionModule = new DetectionModule();
        DetectionStateMachine detectionStateMachine;
        static int success_time = 2000, failure_time = 400, cooldown_time = 2000;

        Mat foreground = null, binaryImage = null, contoursImage = null;
        Mat output;

        static String background_file_name = "background.png";
        // members for timers
        static double fps = 5;
        static int frameNr = 0;

        static int total_time = 5;
        static int timeLeft;
        // timers
        static System.Timers.Timer aTimer;
        static System.Timers.Timer countDownTimer;

        // states
        static int STATE_NORMAL = 0;
        static int STATE_RUN_CAMERA = 1;
        static int STATE_RUN_DETECTION = 2;
        static int STATE_BACKGROUND_SUBSTRACTION = 3;

        // view states
        static int VIEW_STATE_NORMAL = 0;
        static int VIEW_STATE_FOREGROUND = 1;
        static int VIEW_STATE_BINARY = 2;
        static int VIEW_STATE_CONTOURS = 3;

        int state;
        int viewState;

        public MainWindowForm()
        {
            
            InitializeComponent();
            picture_box_main_output.Paint += new PaintEventHandler(Picture_box_paint);

            changeState(STATE_NORMAL);
            loadAppSettings();
            output = frame;
            //Console.WriteLine("fps = " + fps);
        }


        private void Picture_box_paint(object sender, PaintEventArgs e)
        {

            if (state == STATE_RUN_CAMERA)
            {
                videoCapture.Retrieve(frame);
                setPictureBox(output.Bitmap);
                label_fps_value.Text = fps.ToString();
                label_fps_value.Refresh();
            }
            if (state == STATE_RUN_DETECTION)
            {
                videoCapture.Retrieve(frame);
                cameraModule.runCamera(frame,ref foreground,ref binaryImage);
                int sign = detectionModule.runDetection(binaryImage, ref contoursImage, 0);

                detectionStateMachine.insertSign(sign);

                //Console.WriteLine(detectionStateMachine.progress + " %");
                sign = detectionStateMachine.currentDetectedSign;
                if (sign != 0)
                    executeCommand(detectionStateMachine.getSignOnce());

                label_hand_sign.Text = DetectionModule.translate(sign);

                if (viewState == VIEW_STATE_NORMAL)
                    output = frame;
                if (viewState == VIEW_STATE_FOREGROUND)
                    output = foreground;
                if (viewState == VIEW_STATE_BINARY)
                    output = binaryImage;
                if (viewState == VIEW_STATE_CONTOURS)
                    output = contoursImage;

                setPictureBox(output.Bitmap);
                label_fps_value.Text = fps.ToString();
                label_fps_value.Refresh();
                label_hand_sign.Refresh();
                //progressBar_hand_sign.Refresh();
            }
            if (state == STATE_BACKGROUND_SUBSTRACTION)
            {
                videoCapture.Retrieve(frame);
                setPictureBox(frame.Bitmap);
                label_time_left.Text = timeLeft.ToString();
                label_time_left.Refresh();
            }
            if (state == STATE_NORMAL)
            {
                
            }

            menuStrip1.Refresh();
        }


        public void setPictureBox(Image image)
        {
            picture_box_main_output.Image = image;
            frameNr++;
        }


        private void saveCurrentImage()
        {
            CvInvoke.Imwrite(background_file_name, frame);
        }


        void setCountDown()
        {
            timeLeft = total_time;
            if (countDownTimer != null)
                countDownTimer.Stop();
            countDownTimer = new System.Timers.Timer(1000);
            countDownTimer.Elapsed += OnTimedEventCountDown;
            countDownTimer.AutoReset = true;
            countDownTimer.Enabled = true;
        }

        private void OnTimedEventCountDown(Object source, ElapsedEventArgs e)
        {
            if (timeLeft > 0)
                timeLeft--;
            else
            {
                saveCurrentImage();
                countDownTimer.Stop();
                MessageBox.Show("Background image saved!", "background extraction", MessageBoxButtons.OK);

                this.BeginInvoke(new MethodInvoker(delegate     // run on UI thread
                {
                    changeState(STATE_NORMAL);
                }));
            }
        }

        static void SetTimer()
        {
            if (aTimer != null)
                aTimer.Stop();
            aTimer = new System.Timers.Timer(500);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }


        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            fps = frameNr * 2;
            frameNr = 0;
        }

        
        private void changeState(int newState)
        {
            if (newState == state && newState != STATE_NORMAL)
                return;

            state = newState;

            label_fps_text.Visible = false;
            label_fps_value.Visible = false;
            label_time_left.Visible = false;
            viewToolStripMenuItem.Enabled = false;
            label_hand_sign.Visible = false;
            progressBar_hand_sign.Visible = false;
            detectionStateMachine = null;

            if (newState == STATE_NORMAL)
            {
                detectionStateMachine = null;
                changeViewState(VIEW_STATE_NORMAL);
                stopCamera();
                picture_box_main_output.Image = null;
                picture_box_main_output.Refresh();
            }
            if (newState == STATE_RUN_CAMERA)
            {
                changeViewState(VIEW_STATE_NORMAL);
                label_fps_text.Visible = true;
                label_fps_value.Visible = true;
                startCamera();
                picture_box_main_output.Refresh();
            }
            if (newState == STATE_RUN_DETECTION)
            {
                detectionStateMachine = 
                new DetectionStateMachine(success_time, failure_time, cooldown_time, progressBar_hand_sign, this);
                changeViewState(VIEW_STATE_FOREGROUND);
                viewToolStripMenuItem.Enabled = true;
                label_fps_text.Visible = true;
                label_fps_value.Visible = true;
                label_hand_sign.Visible = true;
                progressBar_hand_sign.Visible = true;
                startCamera();
                picture_box_main_output.Refresh();
            }
            if (newState == STATE_BACKGROUND_SUBSTRACTION)
            {
                label_time_left.Visible = true;
                startCamera();
                setCountDown();
                picture_box_main_output.Refresh();
            }

        }

        private void changeViewState(int newViewState)
        {
            
            viewState = newViewState;
            normalToolStripMenuItem.Checked = false;
            foregroundToolStripMenuItem.Checked = false;
            binaryToolStripMenuItem.Checked = false;
            contoursToolStripMenuItem.Checked = false;

            if (newViewState == VIEW_STATE_NORMAL)
            {
                output = frame;
                normalToolStripMenuItem.Checked = true;
            }
            if (newViewState == VIEW_STATE_FOREGROUND)
            {
                foregroundToolStripMenuItem.Checked = true;
            }
            if (newViewState == VIEW_STATE_BINARY)
            {
                binaryToolStripMenuItem.Checked = true;
            }
            if (newViewState == VIEW_STATE_CONTOURS)
            {
                contoursToolStripMenuItem.Checked = true;
            }
        }


        private void startCamera()
        {
            SetTimer();
            if (videoCapture == null)
            {
                videoCapture = new VideoCapture(0);
                videoCapture.Start();
            }
        }

        private void stopCamera()
        {
            if (videoCapture == null)
                return;
            videoCapture.Stop();
            videoCapture.Dispose();
            videoCapture = null;
        }


        public void setCamera(byte[] yBack, byte[] crBack, byte[] cbBack, byte[] ySkin, byte[] crSkin, byte[] cbSkin)
        {
            cameraModule.setCamera(yBack, crBack, cbBack, ySkin, crSkin, cbSkin);
        }

        public void setDetection(int min_area, double medium_distance)
        {
            detectionModule.setDetection(min_area, medium_distance);
        }

        public void setDetectionMachine(int success, int failure, int cooldown)
        {
            success_time = success;
            failure_time = failure;
            cooldown_time = cooldown;
            DetectionStateMachine.setStateMachine(success, failure, cooldown);
        }

        private void loadAppSettings()
        {
            try
            {
                byte[] yback = { (byte)Properties.Settings.Default["yback_min"],
                                    (byte)Properties.Settings.Default["yback_max"]};
                byte[] crback = { (byte)Properties.Settings.Default["crback_min"],
                                    (byte)Properties.Settings.Default["crback_max"]};
                byte[] cbback = { (byte)Properties.Settings.Default["cbback_min"],
                                    (byte)Properties.Settings.Default["cbback_max"]};

                byte[] yskin = { (byte)Properties.Settings.Default["yskin_min"],
                                    (byte)Properties.Settings.Default["yskin_max"]};
                byte[] crskin = { (byte)Properties.Settings.Default["crskin_min"],
                                    (byte)Properties.Settings.Default["crskin_max"]};
                byte[] cbskin = { (byte)Properties.Settings.Default["cbskin_min"],
                                    (byte)Properties.Settings.Default["cbskin_max"]};

                success_time = (int)Properties.Settings.Default["success_interval"];
                failure_time = (int)Properties.Settings.Default["failure_interval"];
                cooldown_time = (int)Properties.Settings.Default["cooldown_interval"];

                int minArea = (int)Properties.Settings.Default["min_area"];
                double distance = (double)Properties.Settings.Default["medium_distance"];
                setDetection(minArea, distance);

                setCamera(yback, crback, cbback, yskin, crskin, cbskin);
            }
            catch (Exception e)
            {

            }
            
        }

        public void executeCommand(int command)
        {
            String filename = OptionsForm.script_dir + OptionsForm.script_names[command];
            if (!Directory.Exists(OptionsForm.script_dir))
            {
                Directory.CreateDirectory(OptionsForm.script_dir);
                File.Create(filename);
                return;
            }
            if (!File.Exists(filename))
            {
                File.Create(filename);
                return;
            }
            if (File.ReadAllText(filename).Trim().Length < 1)
                return;

            Process proc = null;
            try
            {
                proc = new Process();
                
                proc.StartInfo.FileName = filename;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeState(STATE_RUN_CAMERA);
        }

        private void runDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeState(STATE_RUN_DETECTION);
        }

        private void backgroundSubstractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeState(STATE_BACKGROUND_SUBSTRACTION);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (aboutBox == null || aboutBox.IsDisposed)
            {
                aboutBox = new AboutBox1();
                aboutBox.Show();
            }
            else
                aboutBox.BringToFront();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (optionsForm == null || optionsForm.IsDisposed)
            {
                optionsForm = new OptionsForm();
                optionsForm.setCameraOptions(cameraModule.yRangeBackground,
                    cameraModule.crRangeBackground, cameraModule.cbRangeBackground,
                    cameraModule.yRangeSkin, cameraModule.crRangeSkin, cameraModule.cbRangeSkin);
                optionsForm.setDetectionOptions(success_time, failure_time, cooldown_time, 
                    detectionModule.minArea, detectionModule.centersMediumDistance);
                optionsForm.mParent = this;
                optionsForm.Show();
            }
            else
                optionsForm.BringToFront();
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeViewState(VIEW_STATE_NORMAL);
        }

        private void foregroundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeViewState(VIEW_STATE_FOREGROUND);
        }

        private void binaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeViewState(VIEW_STATE_BINARY);
        }

        private void contoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeViewState(VIEW_STATE_CONTOURS);
        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (userHelpForm == null || userHelpForm.IsDisposed)
            {
                userHelpForm = new UserHelp();
                userHelpForm.Show();
            }
            else
                userHelpForm.BringToFront();
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeState(STATE_NORMAL);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Timers;

namespace HandGestureDetector
{
    class Tests
    {
        static int frameNr;
        static double fps = 0;

        public static void initialTest()
        {
            String win1 = "Test Window"; //The name of the window
            CvInvoke.NamedWindow(win1); //Create the window using the specific name

            Mat img = new Mat(200, 400, DepthType.Cv8U, 3); //Create a 3 channel image of 400x200
            img.SetTo(new Bgr(255, 0, 0).MCvScalar); // set it to Blue color

            //Draw "Hello, world." on the image using the specific font
            CvInvoke.PutText(
               img,
               "Hello, world",
               new System.Drawing.Point(10, 80),
               FontFace.HersheyComplex,
               1.0,
               new Bgr(0, 255, 0).MCvScalar);


            CvInvoke.Imshow(win1, img); //Show the image
            CvInvoke.WaitKey(0);  //Wait for the key pressing event
            CvInvoke.DestroyWindow(win1); //Destroy the window if key is pressed
        }

        public static void matTest()
        {
            String win1 = "Test Window"; //The name of the window
            CvInvoke.NamedWindow(win1); //Create the window using the specific name

            Mat img = new Mat(200, 400, DepthType.Cv8U, 3); //Create a 3 channel image of 400x200
            img.SetTo(new Bgr(255, 0, 0).MCvScalar); // set it to Blue color
            Mat piece = new Mat(img, new Rectangle(210, 20, 40, 80) );
            piece.SetTo(new Bgr(0, 0, 0).MCvScalar);
            
            CvInvoke.Imshow(win1, img); //Show the image
            CvInvoke.WaitKey(0);  //Wait for the key pressing event
            CvInvoke.DestroyWindow(win1); //Destroy the window if key is pressed
        }

        public static void cameraTest()
        {
            String win1 = "Camera window"; //The name of the window
            //CvInvoke.NamedWindow(win1, NamedWindowType.Normal); //Create the window using the specific name

            VideoCapture videoCapture = new VideoCapture(0);
            videoCapture.Start();
            Mat frame = new Mat(480, 640, DepthType.Cv8U, 3);

            SetTimer();
            CameraModule cameraModule = new CameraModule();
            DetectionModule detectionModule = new DetectionModule();
            Mat foreground = null, binaryImage = null, contoursImage = null;

            while (true)
            {
                frameNr++;
                videoCapture.Retrieve(frame);

                cameraModule.runCamera(frame, ref foreground, ref binaryImage);
                int sign = detectionModule.runDetection(binaryImage, ref contoursImage, 0); // needs optiomization
                Console.WriteLine(DetectionModule.translate(sign));

                CvInvoke.PutText(foreground, fps.ToString(), new Point(550, 100), FontFace.HersheySimplex, 2, new MCvScalar(255, 0, 255));
                CvInvoke.Imshow(win1, foreground); //Show the image

                if ( (CvInvoke.WaitKey(1) & 0xFF) == 'q' )
                    break;
            }
            videoCapture.Stop();
            CvInvoke.DestroyWindow(win1); //Destroy the window if key is pressed
        }

        static void SetTimer()
        {
            Timer aTimer = new Timer(500);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            fps = frameNr * 2;
            frameNr = 0;
            Console.WriteLine("fps = " + fps);
        }
    }
}

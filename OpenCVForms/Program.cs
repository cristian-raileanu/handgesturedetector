﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

//----------------------------------------------------------------------------
//  Copyright (C) 2004-2017 by EMGU Corporation. All rights reserved.       
//----------------------------------------------------------------------------

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;


namespace HandGestureDetector
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //openCvUI();
            formUI();

        }

        //
        //
        //
        static void formUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            MainWindowForm myForm = new MainWindowForm();
            //myForm.setPictureBox(frame.Bitmap );
            Application.Run(myForm);
        }

        //
        //
        //
        static void openCvUI()
        {
            // chose the test here
            Boolean cameraTest = true;
            if (cameraTest)
                Tests.cameraTest();
            else
                BackgroundExtraction.runBackgroundExtraction();
        }
    }
}

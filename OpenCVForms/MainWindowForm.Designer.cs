﻿namespace HandGestureDetector
{
    partial class MainWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picture_box_main_output = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.applicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundSubstractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.foregroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instructionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_fps_text = new System.Windows.Forms.Label();
            this.label_fps_value = new System.Windows.Forms.Label();
            this.label_time_left = new System.Windows.Forms.Label();
            this.label_hand_sign = new System.Windows.Forms.Label();
            this.progressBar_hand_sign = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_main_output)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // picture_box_main_output
            // 
            this.picture_box_main_output.BackColor = System.Drawing.SystemColors.ControlDark;
            this.picture_box_main_output.Location = new System.Drawing.Point(52, 29);
            this.picture_box_main_output.Name = "picture_box_main_output";
            this.picture_box_main_output.Size = new System.Drawing.Size(640, 480);
            this.picture_box_main_output.TabIndex = 0;
            this.picture_box_main_output.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationToolStripMenuItem,
            this.detectionToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // applicationToolStripMenuItem
            // 
            this.applicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.applicationToolStripMenuItem.Name = "applicationToolStripMenuItem";
            this.applicationToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.applicationToolStripMenuItem.Text = "Application";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // detectionToolStripMenuItem
            // 
            this.detectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem,
            this.runDetectionToolStripMenuItem,
            this.backgroundSubstractionToolStripMenuItem,
            this.stopToolStripMenuItem});
            this.detectionToolStripMenuItem.Name = "detectionToolStripMenuItem";
            this.detectionToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.detectionToolStripMenuItem.Text = "Detection";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.runToolStripMenuItem.Text = "Run Camera";
            this.runToolStripMenuItem.Click += new System.EventHandler(this.runToolStripMenuItem_Click);
            // 
            // runDetectionToolStripMenuItem
            // 
            this.runDetectionToolStripMenuItem.Name = "runDetectionToolStripMenuItem";
            this.runDetectionToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.runDetectionToolStripMenuItem.Text = "Run Detection";
            this.runDetectionToolStripMenuItem.Click += new System.EventHandler(this.runDetectionToolStripMenuItem_Click);
            // 
            // backgroundSubstractionToolStripMenuItem
            // 
            this.backgroundSubstractionToolStripMenuItem.Name = "backgroundSubstractionToolStripMenuItem";
            this.backgroundSubstractionToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.backgroundSubstractionToolStripMenuItem.Text = "Background substraction";
            this.backgroundSubstractionToolStripMenuItem.Click += new System.EventHandler(this.backgroundSubstractionToolStripMenuItem_Click);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            this.stopToolStripMenuItem.Click += new System.EventHandler(this.stopToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalToolStripMenuItem,
            this.foregroundToolStripMenuItem,
            this.binaryToolStripMenuItem,
            this.contoursToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.normalToolStripMenuItem.Text = "Normal";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // foregroundToolStripMenuItem
            // 
            this.foregroundToolStripMenuItem.Checked = true;
            this.foregroundToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.foregroundToolStripMenuItem.Name = "foregroundToolStripMenuItem";
            this.foregroundToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.foregroundToolStripMenuItem.Text = "Foreground";
            this.foregroundToolStripMenuItem.Click += new System.EventHandler(this.foregroundToolStripMenuItem_Click);
            // 
            // binaryToolStripMenuItem
            // 
            this.binaryToolStripMenuItem.Name = "binaryToolStripMenuItem";
            this.binaryToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.binaryToolStripMenuItem.Text = "Binary";
            this.binaryToolStripMenuItem.Click += new System.EventHandler(this.binaryToolStripMenuItem_Click);
            // 
            // contoursToolStripMenuItem
            // 
            this.contoursToolStripMenuItem.Name = "contoursToolStripMenuItem";
            this.contoursToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.contoursToolStripMenuItem.Text = "Contours";
            this.contoursToolStripMenuItem.Click += new System.EventHandler(this.contoursToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instructionsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // instructionsToolStripMenuItem
            // 
            this.instructionsToolStripMenuItem.Name = "instructionsToolStripMenuItem";
            this.instructionsToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.instructionsToolStripMenuItem.Text = "Instructions";
            this.instructionsToolStripMenuItem.Click += new System.EventHandler(this.instructionsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label_fps_text
            // 
            this.label_fps_text.AutoSize = true;
            this.label_fps_text.Location = new System.Drawing.Point(726, 53);
            this.label_fps_text.Name = "label_fps_text";
            this.label_fps_text.Size = new System.Drawing.Size(21, 13);
            this.label_fps_text.TabIndex = 2;
            this.label_fps_text.Text = "fps";
            this.label_fps_text.Visible = false;
            // 
            // label_fps_value
            // 
            this.label_fps_value.AutoSize = true;
            this.label_fps_value.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label_fps_value.Location = new System.Drawing.Point(698, 53);
            this.label_fps_value.Name = "label_fps_value";
            this.label_fps_value.Size = new System.Drawing.Size(22, 13);
            this.label_fps_value.TabIndex = 3;
            this.label_fps_value.Text = "0.0";
            this.label_fps_value.Visible = false;
            // 
            // label_time_left
            // 
            this.label_time_left.AutoSize = true;
            this.label_time_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_time_left.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label_time_left.Location = new System.Drawing.Point(698, 174);
            this.label_time_left.Name = "label_time_left";
            this.label_time_left.Size = new System.Drawing.Size(70, 76);
            this.label_time_left.TabIndex = 4;
            this.label_time_left.Text = "0";
            this.label_time_left.Visible = false;
            // 
            // label_hand_sign
            // 
            this.label_hand_sign.AutoSize = true;
            this.label_hand_sign.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_hand_sign.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label_hand_sign.Location = new System.Drawing.Point(361, 512);
            this.label_hand_sign.Name = "label_hand_sign";
            this.label_hand_sign.Size = new System.Drawing.Size(171, 39);
            this.label_hand_sign.TabIndex = 5;
            this.label_hand_sign.Text = "Hand sign";
            this.label_hand_sign.Visible = false;
            // 
            // progressBar_hand_sign
            // 
            this.progressBar_hand_sign.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.progressBar_hand_sign.Location = new System.Drawing.Point(52, 520);
            this.progressBar_hand_sign.Name = "progressBar_hand_sign";
            this.progressBar_hand_sign.Size = new System.Drawing.Size(303, 23);
            this.progressBar_hand_sign.Step = 5;
            this.progressBar_hand_sign.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar_hand_sign.TabIndex = 6;
            // 
            // MainWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(784, 555);
            this.Controls.Add(this.progressBar_hand_sign);
            this.Controls.Add(this.label_hand_sign);
            this.Controls.Add(this.label_time_left);
            this.Controls.Add(this.label_fps_value);
            this.Controls.Add(this.label_fps_text);
            this.Controls.Add(this.picture_box_main_output);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainWindowForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Hand Detector";
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_main_output)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picture_box_main_output;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem applicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instructionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label_fps_text;
        private System.Windows.Forms.Label label_fps_value;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundSubstractionToolStripMenuItem;
        private System.Windows.Forms.Label label_time_left;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem foregroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem binaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contoursToolStripMenuItem;
        private System.Windows.Forms.Label label_hand_sign;
        private System.Windows.Forms.ProgressBar progressBar_hand_sign;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HandGestureDetector
{
    public partial class OptionsForm : Form
    {
        public MainWindowForm mParent;
        private ScriptEditForm scriptEditForm;

        public static String script_dir = "scripts\\";
        public static String[] script_names = {"0", "closed_script.bat",
            "pointing_script.bat", "gun_script.bat", "pinch_script.bat", "ok_script.bat",
            "claw_script.bat", "open_script.bat", "fingers_2.bat", "fingers_3.bat", "fingers_4.bat",
            "sign_o.bat"};

        public OptionsForm()
        {
            InitializeComponent();
            tabControl1.DrawItem += TabControl1_DrawItem;
        }

        private void TabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            String tabTitle = tabControl1.TabPages[e.Index].Text;
            SizeF textSize = g.MeasureString(tabTitle, tabControl1.Font);
            int iX = e.Bounds.Left + 6;
            int iY = e.Bounds.Top + (e.Bounds.Height - (int)textSize.Height) / 2;

            if (tabControl1.SelectedIndex == e.Index)
                tabControl1.TabPages[e.Index].BackColor = tabCamera.BackColor;
            //g.FillRectangle(new SolidBrush(tabCamera.BackColor), 
            //    tabControl1.TabPages[e.Index].ClientRectangle);
            if (tabControl1.SelectedIndex == e.Index)
                g.DrawString(tabTitle, tabControl1.Font, Brushes.Blue, iX, iY);
            else
                g.DrawString(tabTitle, tabControl1.Font, Brushes.Black, iX, iY);
        }

        public void setCameraOptions(byte[] yBack, byte[] crBack, byte[] cbBack, byte[] ySkin, byte[] crSkin, byte[] cbSkin)
        {
            sliderValuesY.setSelectedRange(yBack[0], yBack[1]);
            sliderValuesCr.setSelectedRange(crBack[0], crBack[1]);
            sliderValuesCb.setSelectedRange(cbBack[0], cbBack[1]);
            sliderValueSkinY.setSelectedRange(ySkin[0], ySkin[1]);
            sliderValueSkinCr.setSelectedRange(crSkin[0], crSkin[1]);
            sliderValueSkinCb.setSelectedRange(cbSkin[0], cbSkin[1]);
        }

        public void setDetectionOptions(int success, int failure, int cooldown, int min_area, double medium_distance)
        {
            numeric_success.Value = success;
            numeric_failure.Value = failure;
            numeric_cooldown.Value = cooldown;
            numeric_min_area.Value = min_area;
            int value = (int)Math.Round(medium_distance * 100);
            numeric_medium_distance.Value = value;
        }

        private void applySettings()
        {
            byte[] yback = { (byte)sliderValuesY.getSelectedMin(), (byte)sliderValuesY.getSelectedMax() };
            byte[] crback = { (byte)sliderValuesCr.getSelectedMin(), (byte)sliderValuesCr.getSelectedMax() };
            byte[] cbback = { (byte)sliderValuesCb.getSelectedMin(), (byte)sliderValuesCb.getSelectedMax() };

            byte[] yskin = { (byte)sliderValueSkinY.getSelectedMin(), (byte)sliderValueSkinY.getSelectedMax() };
            byte[] crskin = { (byte)sliderValueSkinCr.getSelectedMin(), (byte)sliderValueSkinCr.getSelectedMax() };
            byte[] cbskin = { (byte)sliderValueSkinCb.getSelectedMin(), (byte)sliderValueSkinCb.getSelectedMax() };
            mParent.setCamera(yback, crback, cbback, yskin, crskin, cbskin);
            
            Properties.Settings.Default["yback_min"] = yback[0];
            Properties.Settings.Default["yback_max"] = yback[1];
            Properties.Settings.Default["crback_min"] = crback[0];
            Properties.Settings.Default["crback_max"] = crback[1];
            Properties.Settings.Default["cbback_min"] = cbback[0];
            Properties.Settings.Default["cbback_max"] = cbback[1];

            Properties.Settings.Default["yskin_min"] = yskin[0];
            Properties.Settings.Default["yskin_max"] = yskin[1];
            Properties.Settings.Default["crskin_min"] = crskin[0];
            Properties.Settings.Default["crskin_max"] = crskin[1];
            Properties.Settings.Default["cbskin_min"] = cbskin[0];
            Properties.Settings.Default["cbskin_max"] = cbskin[1];

            int success = (int)numeric_success.Value, failure = (int)numeric_failure.Value;
            int cooldown = (int)numeric_cooldown.Value;
            Properties.Settings.Default["success_interval"] = success;
            Properties.Settings.Default["failure_interval"] = failure;
            Properties.Settings.Default["cooldown_interval"] = cooldown;
            mParent.setDetectionMachine(success, failure, cooldown);

            int minArea = (int)numeric_min_area.Value;
            double mediumDist = (double)numeric_medium_distance.Value / 100.0;
            Properties.Settings.Default["min_area"] = minArea;
            Properties.Settings.Default["medium_distance"] = mediumDist;
            mParent.setDetection(minArea, mediumDist);

            Properties.Settings.Default.Save();
        }


        
        void assureFileExistence(String folder, String filename)
        {
            String path = folder + filename;
            if(!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (!File.Exists(path))
                File.Create(path);
        }

        public void saveCommand(String filename, int command)
        {
            assureFileExistence(script_dir, script_names[command]);
            if (filename.Length > 0)
            {
                File.Copy(filename, script_dir + script_names[command], true);
            }
        }

        void saveScript(String script, int command)
        {
            File.WriteAllText(script_dir + script_names[command], script);
        }

        void clearScript(int command)
        {
            File.WriteAllText(script_dir + script_names[command], "");
        }

        String loadScript(int command)
        {
            assureFileExistence(script_dir, script_names[command]);
            String content = File.ReadAllText(script_dir + script_names[command]);
            return content;
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool hasScript(int command)
        {
            String path = script_dir + script_names[command];
            if (!File.Exists(path))
                return false;

            if (File.ReadAllText(path).Length < 1)
                return false;

            return true;
        }
        

        private void buttonApply_Click(object sender, EventArgs e)
        {
            applySettings();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            applySettings();
            Close();
        }

        private void btn_upload_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Panel panel = (Panel)btn.Parent;
            //Console.WriteLine("tab index= "+panel.TabIndex);
            int command = panel.TabIndex;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files | *.bat";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                saveCommand(openFileDialog.FileName, command);
                panel.Refresh();
            }
        }

        private void label_sign_title_Click(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            Panel panel = (Panel)label.Parent;
            int command = panel.TabIndex;
            mParent.executeCommand(command);
        }

        private void button_create_script_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Panel panel = (Panel)btn.Parent;
            int command = panel.TabIndex;

            if (scriptEditForm == null)
                scriptEditForm = new ScriptEditForm(this);
            scriptEditForm.setText(loadScript(command));
            if (scriptEditForm.ShowDialog() == DialogResult.OK)
            {
                saveScript( scriptEditForm.text , command);
                panel.Refresh();
            }
        }

        private void button_clear_script_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Panel panel = (Panel)btn.Parent;
            int command = panel.TabIndex;
            clearScript(command);
            panel.Refresh();
        }

        private void label_script_status_repaint(object sender, PaintEventArgs e)
        {
            Label label = (Label)sender;
            Panel panel = (Panel)label.Parent;
            int command = panel.TabIndex;

            //Console.WriteLine("repaint!!");
            if (hasScript(command))
                label.Text = "Has script";
            else
                label.Text = "No script has been set";
        }

        private void flowLayout_control_added(object sender, ControlEventArgs e)
        {

        }
    }
}

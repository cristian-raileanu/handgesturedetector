﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Timers;

namespace HandGestureDetector
{
    class BackgroundExtraction
    {
        private static System.Timers.Timer aTimer;
        private static int timeLeft;
        static private String backgroundFileName = "background.png";

        public static void runBackgroundExtraction()
        {
            String win1 = "Camera window"; //The name of the window
            CvInvoke.NamedWindow(win1); //Create the window using the specific name

            VideoCapture videoCapture = new VideoCapture();
            videoCapture.Start();
            timeLeft = 6;
            SetTimer();
            //while (True):
            Mat frame = new Mat();
            while (true)
            {
                videoCapture.Retrieve(frame);

                if (timeLeft > 0)
                {
                    FontFace font = FontFace.HersheySimplex;
                    String message = "Get out of the frame";
                    CvInvoke.PutText(frame, message, new Point(100, 50), font, 1, new MCvScalar(0, 0, 255));
                    CvInvoke.PutText(frame, timeLeft.ToString(), new Point(550, 100), font, 3, new MCvScalar(255, 0, 255));
                }
                else
                {
                    CvInvoke.Imwrite(backgroundFileName, frame);
                    break;
                }

                CvInvoke.Imshow(win1, frame); //Show the image

                if((CvInvoke.WaitKey(1) & 0xFF) == 'q')
                    break;
            }
            videoCapture.Stop();
            CvInvoke.DestroyWindow(win1); //Destroy the window if key is pressed
        }

        static void SetTimer()
        {
            aTimer = new Timer(1000);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            timeLeft--;
        }
    }
}

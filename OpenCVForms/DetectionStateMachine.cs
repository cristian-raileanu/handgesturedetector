﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace HandGestureDetector
{
    class DetectionStateMachine
    {
        static int NOTHING_SIGN = 0;
        static int total_time_success = 1000;
        static int total_time_failure = 200;
        static int total_time_cooldown = 500;

        System.Windows.Forms.ProgressBar mProgressbar;
        System.Windows.Forms.Form mUiForm;

        private Timer tryFailureTimer;
        private Timer coolDownTimer;

        // out
        public int progress = 0;
        public int currentDetectedSign = 0;

        private int hypothesis = 0;
        private int good_signs;
        private int bad_signs;
        private int progressStepValue;
        private bool signReady;

        public DetectionStateMachine(int time_success, int time_failure, int cooldown, 
            System.Windows.Forms.ProgressBar progressbar, System.Windows.Forms.Form parentForm)
        {
            setStateMachine(time_success, time_failure, cooldown);
            progressStepValue = total_time_failure * 100 / total_time_success;
            mProgressbar = progressbar;
            mProgressbar.Step = progressStepValue;
            mUiForm = parentForm;
        }

        public static void setStateMachine(int success, int failure, int cooldown)
        {
            total_time_success = success;
            total_time_failure = failure;
            total_time_cooldown = cooldown;
        }

        public void insertSign(int sign)
        {
            if (sign == 0)
                return;

            if (hypothesis != NOTHING_SIGN)
            {
                if (sign == hypothesis)
                    good_signs++;
                else
                    bad_signs++;
                //Console.WriteLine("<Insert> bad: " + bad_signs + "; good: " + good_signs);
            }
            else
                if (sign != NOTHING_SIGN)
                {
                    hypothesis = sign;
                    reset();
                    good_signs++;
                }
        }

        public int getSignOnce()
        {
            if (currentDetectedSign != NOTHING_SIGN && signReady)
            {
                signReady = false;
                return currentDetectedSign;
            }
            else
                return NOTHING_SIGN;

        }

        private void reset()
        {
            //Console.WriteLine("reset!");
            changeProgress(0);
            good_signs = 0;
            bad_signs = 0;
            currentDetectedSign = NOTHING_SIGN;
            setCountDownFailure();
        }

        private void stop()
        {
            //Console.WriteLine("stop timer! ");
            changeProgress(0);
            signReady = false;
            hypothesis = NOTHING_SIGN;
            currentDetectedSign = 0;
            bad_signs = 0;
            good_signs = 0;
            if (tryFailureTimer != null)
                tryFailureTimer.Stop();
        }

        private void resetTimer(Timer timer, int time, ElapsedEventHandler elapsedHandler, bool autoreset)
        {
            if (timer != null)
                timer.Stop();
            timer = new Timer(time);
            timer.Elapsed += elapsedHandler;
            timer.AutoReset = autoreset;
            timer.Enabled = true;
        }

        

        void setCountDownFailure()
        {
            //Console.WriteLine("START TIMER! ");
            resetTimer(tryFailureTimer, total_time_failure, TryFailureTimer_Elapsed, false);
        }

        private void changeProgress(int newProgressValue)
        {
            progress = newProgressValue;
            mUiForm.BeginInvoke(new System.Windows.Forms.MethodInvoker(delegate     // run on UI thread
            {
                //if (newProgressValue == 99)
                //    mProgressbar.Increment(progressStepValue);
                //else
                    mProgressbar.Value = newProgressValue;
            }));
        }

        private void TryFailureTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Console.WriteLine("<Timercycle> bad: " + bad_signs + "; good: " + good_signs);

            if (bad_signs >= good_signs) // FAILURE CONDITION
                stop();
            else
            { 
                bad_signs = 0;
                good_signs = 0;
                if (progress + progressStepValue <= 99)
                    changeProgress(progress + progressStepValue);
                else
                    changeProgress(99);

                if (progress == 99)
                {
                    currentDetectedSign = hypothesis;
                    signReady = true;
                    if (tryFailureTimer != null)
                        tryFailureTimer.Stop();
                    resetTimer(coolDownTimer, total_time_cooldown, CoolDownTimer_Elapsed, false);
                }
                else
                    setCountDownFailure();
            }
        }

       
        private void CoolDownTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            stop();
        }
    }
}

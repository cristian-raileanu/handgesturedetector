﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HandGestureDetector
{
    public partial class ScriptEditForm : Form
    {
        OptionsForm mParent;
        public String text = "";

        public ScriptEditForm(OptionsForm parent)
        {
            InitializeComponent();
            mParent = parent;
        }

        public void setText(String _text)
        {
            textBoxScript.Text = _text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            text = textBoxScript.Text;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}

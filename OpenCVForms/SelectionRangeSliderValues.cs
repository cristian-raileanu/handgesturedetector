﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HandGestureDetector
{
    public partial class SelectionRangeSliderValues : UserControl
    {
        public SelectionRangeSliderValues()
        {
            InitializeComponent();
            selectionRangeSlider1.Paint += SelectionRangeSliderValues_Paint;
        }

        private void SelectionRangeSliderValues_Paint(object sender, PaintEventArgs e)
        {
            int min = selectionRangeSlider1.SelectedMin;
            int max = selectionRangeSlider1.SelectedMax;
            Console.WriteLine(min+" "+max);
            label_min.Text = min.ToString();
            label_max.Text = max.ToString();
            //Refresh();
        }

        public void setSelectedRange(int min, int max)
        {
            selectionRangeSlider1.SelectedMin = min;
            selectionRangeSlider1.SelectedMax = max;
        }

        public int getSelectedMin()
        {
            return selectionRangeSlider1.SelectedMin;
        }

        public int getSelectedMax()
        {
            return selectionRangeSlider1.SelectedMax;
        }
    }
}

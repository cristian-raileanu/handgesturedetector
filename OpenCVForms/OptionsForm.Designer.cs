﻿namespace HandGestureDetector
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCamera = new System.Windows.Forms.TabPage();
            this.groupBoxSkin = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDetection = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numeric_medium_distance = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.numeric_min_area = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numeric_cooldown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numeric_failure = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numeric_success = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tabOutput = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_script_status = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button_clear_script = new System.Windows.Forms.Button();
            this.btn_upload_open = new System.Windows.Forms.Button();
            this.button_create_script = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.sliderValueSkinCb = new HandGestureDetector.SelectionRangeSliderValues();
            this.sliderValueSkinCr = new HandGestureDetector.SelectionRangeSliderValues();
            this.sliderValueSkinY = new HandGestureDetector.SelectionRangeSliderValues();
            this.sliderValuesCb = new HandGestureDetector.SelectionRangeSliderValues();
            this.sliderValuesCr = new HandGestureDetector.SelectionRangeSliderValues();
            this.sliderValuesY = new HandGestureDetector.SelectionRangeSliderValues();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabCamera.SuspendLayout();
            this.groupBoxSkin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabDetection.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_medium_distance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_min_area)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_cooldown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_failure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_success)).BeginInit();
            this.tabOutput.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tabCamera);
            this.tabControl1.Controls.Add(this.tabDetection);
            this.tabControl1.Controls.Add(this.tabOutput);
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.ItemSize = new System.Drawing.Size(30, 80);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(460, 445);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabCamera
            // 
            this.tabCamera.BackColor = System.Drawing.Color.DarkGray;
            this.tabCamera.Controls.Add(this.groupBoxSkin);
            this.tabCamera.Controls.Add(this.groupBox1);
            this.tabCamera.Location = new System.Drawing.Point(84, 4);
            this.tabCamera.Margin = new System.Windows.Forms.Padding(0);
            this.tabCamera.Name = "tabCamera";
            this.tabCamera.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabCamera.Size = new System.Drawing.Size(372, 437);
            this.tabCamera.TabIndex = 0;
            this.tabCamera.Text = "Camera";
            // 
            // groupBoxSkin
            // 
            this.groupBoxSkin.Controls.Add(this.sliderValueSkinCb);
            this.groupBoxSkin.Controls.Add(this.label5);
            this.groupBoxSkin.Controls.Add(this.sliderValueSkinCr);
            this.groupBoxSkin.Controls.Add(this.label6);
            this.groupBoxSkin.Controls.Add(this.sliderValueSkinY);
            this.groupBoxSkin.Controls.Add(this.label7);
            this.groupBoxSkin.Location = new System.Drawing.Point(24, 155);
            this.groupBoxSkin.Name = "groupBoxSkin";
            this.groupBoxSkin.Size = new System.Drawing.Size(334, 118);
            this.groupBoxSkin.TabIndex = 5;
            this.groupBoxSkin.TabStop = false;
            this.groupBoxSkin.Text = "Skin Extraction";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(18, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Cb";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(18, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Cr";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(18, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Y";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sliderValuesCb);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.sliderValuesCr);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.sliderValuesY);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(24, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(334, 118);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Background extraction";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(18, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cb";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(18, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Cr";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(18, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // tabDetection
            // 
            this.tabDetection.BackColor = System.Drawing.Color.DarkGray;
            this.tabDetection.Controls.Add(this.groupBox3);
            this.tabDetection.Controls.Add(this.groupBox2);
            this.tabDetection.Location = new System.Drawing.Point(84, 4);
            this.tabDetection.Name = "tabDetection";
            this.tabDetection.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetection.Size = new System.Drawing.Size(372, 437);
            this.tabDetection.TabIndex = 1;
            this.tabDetection.Text = "Detection";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numeric_medium_distance);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.numeric_min_area);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Location = new System.Drawing.Point(17, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(341, 110);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detection";
            // 
            // numeric_medium_distance
            // 
            this.numeric_medium_distance.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numeric_medium_distance.Location = new System.Drawing.Point(210, 51);
            this.numeric_medium_distance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_medium_distance.Name = "numeric_medium_distance";
            this.numeric_medium_distance.Size = new System.Drawing.Size(87, 20);
            this.numeric_medium_distance.TabIndex = 4;
            this.numeric_medium_distance.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(15, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(189, 17);
            this.label18.TabIndex = 3;
            this.label18.Text = "Centers medium distance(%)";
            // 
            // numeric_min_area
            // 
            this.numeric_min_area.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric_min_area.Location = new System.Drawing.Point(210, 25);
            this.numeric_min_area.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numeric_min_area.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numeric_min_area.Name = "numeric_min_area";
            this.numeric_min_area.Size = new System.Drawing.Size(87, 20);
            this.numeric_min_area.TabIndex = 2;
            this.numeric_min_area.ThousandsSeparator = true;
            this.numeric_min_area.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(15, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(132, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "Minimum hand area";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numeric_cooldown);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.numeric_failure);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.numeric_success);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(17, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(341, 110);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detection state machine";
            // 
            // numeric_cooldown
            // 
            this.numeric_cooldown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric_cooldown.Location = new System.Drawing.Point(210, 78);
            this.numeric_cooldown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numeric_cooldown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric_cooldown.Name = "numeric_cooldown";
            this.numeric_cooldown.Size = new System.Drawing.Size(87, 20);
            this.numeric_cooldown.TabIndex = 6;
            this.numeric_cooldown.ThousandsSeparator = true;
            this.numeric_cooldown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(15, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "Cooldown time (ms):";
            // 
            // numeric_failure
            // 
            this.numeric_failure.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric_failure.Location = new System.Drawing.Point(210, 51);
            this.numeric_failure.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numeric_failure.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric_failure.Name = "numeric_failure";
            this.numeric_failure.Size = new System.Drawing.Size(87, 20);
            this.numeric_failure.TabIndex = 4;
            this.numeric_failure.ThousandsSeparator = true;
            this.numeric_failure.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(15, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Failure time (ms):";
            // 
            // numeric_success
            // 
            this.numeric_success.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric_success.Location = new System.Drawing.Point(210, 25);
            this.numeric_success.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numeric_success.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric_success.Name = "numeric_success";
            this.numeric_success.Size = new System.Drawing.Size(87, 20);
            this.numeric_success.TabIndex = 2;
            this.numeric_success.ThousandsSeparator = true;
            this.numeric_success.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(15, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Success time (ms):";
            // 
            // tabOutput
            // 
            this.tabOutput.BackColor = System.Drawing.Color.DarkGray;
            this.tabOutput.Controls.Add(this.label21);
            this.tabOutput.Controls.Add(this.flowLayoutPanel1);
            this.tabOutput.Location = new System.Drawing.Point(84, 4);
            this.tabOutput.Margin = new System.Windows.Forms.Padding(0);
            this.tabOutput.Name = "tabOutput";
            this.tabOutput.Size = new System.Drawing.Size(372, 437);
            this.tabOutput.TabIndex = 2;
            this.tabOutput.Text = "Output";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 5);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(292, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Hint: by clicking on the title you can test how the scripts runs";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.Controls.Add(this.panel7);
            this.flowLayoutPanel1.Controls.Add(this.panel8);
            this.flowLayoutPanel1.Controls.Add(this.panel6);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(369, 441);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightGray;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(366, 82);
            this.panel2.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label11.Location = new System.Drawing.Point(91, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(252, 29);
            this.label11.TabIndex = 5;
            this.label11.Tag = "5";
            this.label11.Text = "No script has been set";
            this.label11.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(93, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 17);
            this.label12.TabIndex = 0;
            this.label12.Tag = "1";
            this.label12.Text = "Closed hand";
            this.label12.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(259, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Tag = "4";
            this.button3.Text = "clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4.Location = new System.Drawing.Point(96, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 1;
            this.button4.Tag = "2";
            this.button4.Text = "upload file";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(178, 23);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Tag = "3";
            this.button5.Text = "create script";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.LightGray;
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.pictureBox5);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.button12);
            this.panel5.Controls.Add(this.button13);
            this.panel5.Controls.Add(this.button14);
            this.panel5.Location = new System.Drawing.Point(3, 91);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(366, 82);
            this.panel5.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label17.Location = new System.Drawing.Point(91, 49);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(252, 29);
            this.label17.TabIndex = 5;
            this.label17.Tag = "5";
            this.label17.Text = "No script has been set";
            this.label17.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(93, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 17);
            this.label20.TabIndex = 0;
            this.label20.Tag = "1";
            this.label20.Text = "Pointing";
            this.label20.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(259, 23);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 3;
            this.button12.Tag = "4";
            this.button12.Text = "clear";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button13.Location = new System.Drawing.Point(96, 23);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 1;
            this.button13.Tag = "2";
            this.button13.Text = "upload file";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(178, 23);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 2;
            this.button14.Tag = "3";
            this.button14.Text = "create script";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.LightGray;
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.pictureBox6);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.button15);
            this.panel6.Controls.Add(this.button16);
            this.panel6.Controls.Add(this.button17);
            this.panel6.Location = new System.Drawing.Point(3, 355);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(366, 82);
            this.panel6.TabIndex = 6;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label22.Location = new System.Drawing.Point(91, 49);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(252, 29);
            this.label22.TabIndex = 5;
            this.label22.Tag = "5";
            this.label22.Text = "No script has been set";
            this.label22.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.Location = new System.Drawing.Point(93, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 17);
            this.label23.TabIndex = 0;
            this.label23.Tag = "1";
            this.label23.Text = "Claw";
            this.label23.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(259, 23);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 3;
            this.button15.Tag = "4";
            this.button15.Text = "clear";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button16.Location = new System.Drawing.Point(96, 23);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 1;
            this.button16.Tag = "2";
            this.button16.Text = "upload file";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(178, 23);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 2;
            this.button17.Tag = "3";
            this.button17.Text = "create script";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.label_script_status);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.button_clear_script);
            this.panel1.Controls.Add(this.btn_upload_open);
            this.panel1.Controls.Add(this.button_create_script);
            this.panel1.Location = new System.Drawing.Point(3, 443);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(366, 82);
            this.panel1.TabIndex = 7;
            // 
            // label_script_status
            // 
            this.label_script_status.AutoSize = true;
            this.label_script_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_script_status.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_script_status.Location = new System.Drawing.Point(91, 49);
            this.label_script_status.Name = "label_script_status";
            this.label_script_status.Size = new System.Drawing.Size(252, 29);
            this.label_script_status.TabIndex = 5;
            this.label_script_status.Tag = "5";
            this.label_script_status.Text = "No script has been set";
            this.label_script_status.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(93, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 0;
            this.label8.Tag = "1";
            this.label8.Text = "Open hand";
            this.label8.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button_clear_script
            // 
            this.button_clear_script.Location = new System.Drawing.Point(259, 23);
            this.button_clear_script.Name = "button_clear_script";
            this.button_clear_script.Size = new System.Drawing.Size(75, 23);
            this.button_clear_script.TabIndex = 3;
            this.button_clear_script.Tag = "4";
            this.button_clear_script.Text = "clear";
            this.button_clear_script.UseVisualStyleBackColor = true;
            this.button_clear_script.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // btn_upload_open
            // 
            this.btn_upload_open.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_upload_open.Location = new System.Drawing.Point(96, 23);
            this.btn_upload_open.Name = "btn_upload_open";
            this.btn_upload_open.Size = new System.Drawing.Size(75, 23);
            this.btn_upload_open.TabIndex = 1;
            this.btn_upload_open.Tag = "2";
            this.btn_upload_open.Text = "upload file";
            this.btn_upload_open.UseVisualStyleBackColor = true;
            this.btn_upload_open.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button_create_script
            // 
            this.button_create_script.Location = new System.Drawing.Point(178, 23);
            this.button_create_script.Name = "button_create_script";
            this.button_create_script.Size = new System.Drawing.Size(75, 23);
            this.button_create_script.TabIndex = 2;
            this.button_create_script.Tag = "3";
            this.button_create_script.Text = "create script";
            this.button_create_script.UseVisualStyleBackColor = true;
            this.button_create_script.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightGray;
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.button7);
            this.panel3.Controls.Add(this.button8);
            this.panel3.Location = new System.Drawing.Point(3, 531);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(366, 82);
            this.panel3.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label13.Location = new System.Drawing.Point(91, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(252, 29);
            this.label13.TabIndex = 5;
            this.label13.Tag = "5";
            this.label13.Text = "No script has been set";
            this.label13.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(93, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 17);
            this.label14.TabIndex = 0;
            this.label14.Tag = "1";
            this.label14.Text = "2 fingers";
            this.label14.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(259, 23);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 3;
            this.button6.Tag = "4";
            this.button6.Text = "clear";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button7.Location = new System.Drawing.Point(96, 23);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 1;
            this.button7.Tag = "2";
            this.button7.Text = "upload file";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(178, 23);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 2;
            this.button8.Tag = "3";
            this.button8.Text = "create script";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.LightGray;
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.pictureBox4);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.button9);
            this.panel4.Controls.Add(this.button10);
            this.panel4.Controls.Add(this.button11);
            this.panel4.Location = new System.Drawing.Point(3, 619);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(366, 82);
            this.panel4.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label15.Location = new System.Drawing.Point(91, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(252, 29);
            this.label15.TabIndex = 5;
            this.label15.Tag = "5";
            this.label15.Text = "No script has been set";
            this.label15.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(93, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 17);
            this.label16.TabIndex = 0;
            this.label16.Tag = "1";
            this.label16.Text = "3 fingers";
            this.label16.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(259, 23);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 3;
            this.button9.Tag = "4";
            this.button9.Text = "clear";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button10.Location = new System.Drawing.Point(96, 23);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 1;
            this.button10.Tag = "2";
            this.button10.Text = "upload file";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(178, 23);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 2;
            this.button11.Tag = "3";
            this.button11.Text = "create script";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(131, 453);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(212, 453);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 12;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(293, 453);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.LightGray;
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.pictureBox7);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Controls.Add(this.button18);
            this.panel7.Controls.Add(this.button19);
            this.panel7.Controls.Add(this.button20);
            this.panel7.Location = new System.Drawing.Point(3, 179);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(366, 82);
            this.panel7.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label24.Location = new System.Drawing.Point(91, 49);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(252, 29);
            this.label24.TabIndex = 5;
            this.label24.Tag = "5";
            this.label24.Text = "No script has been set";
            this.label24.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.Location = new System.Drawing.Point(93, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(26, 17);
            this.label25.TabIndex = 0;
            this.label25.Tag = "1";
            this.label25.Text = "Ok";
            this.label25.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(259, 23);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 3;
            this.button18.Tag = "4";
            this.button18.Text = "clear";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button19.Location = new System.Drawing.Point(96, 23);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 1;
            this.button19.Tag = "2";
            this.button19.Text = "upload file";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(178, 23);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 2;
            this.button20.Tag = "3";
            this.button20.Text = "create script";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.LightGray;
            this.panel8.Controls.Add(this.label26);
            this.panel8.Controls.Add(this.pictureBox8);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.button21);
            this.panel8.Controls.Add(this.button22);
            this.panel8.Controls.Add(this.button23);
            this.panel8.Location = new System.Drawing.Point(3, 267);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(366, 82);
            this.panel8.TabIndex = 11;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label26.Location = new System.Drawing.Point(91, 49);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(252, 29);
            this.label26.TabIndex = 5;
            this.label26.Tag = "5";
            this.label26.Text = "No script has been set";
            this.label26.Paint += new System.Windows.Forms.PaintEventHandler(this.label_script_status_repaint);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.Location = new System.Drawing.Point(93, 3);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 17);
            this.label27.TabIndex = 0;
            this.label27.Tag = "1";
            this.label27.Text = "O";
            this.label27.Click += new System.EventHandler(this.label_sign_title_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(259, 23);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 3;
            this.button21.Tag = "4";
            this.button21.Text = "clear";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button_clear_script_Click);
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button22.Location = new System.Drawing.Point(96, 23);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 1;
            this.button22.Tag = "2";
            this.button22.Text = "upload file";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(178, 23);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 2;
            this.button23.Tag = "3";
            this.button23.Text = "create script";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button_create_script_Click);
            // 
            // sliderValueSkinCb
            // 
            this.sliderValueSkinCb.AutoSize = true;
            this.sliderValueSkinCb.Location = new System.Drawing.Point(70, 87);
            this.sliderValueSkinCb.Name = "sliderValueSkinCb";
            this.sliderValueSkinCb.Size = new System.Drawing.Size(258, 25);
            this.sliderValueSkinCb.TabIndex = 7;
            // 
            // sliderValueSkinCr
            // 
            this.sliderValueSkinCr.AutoSize = true;
            this.sliderValueSkinCr.Location = new System.Drawing.Point(70, 57);
            this.sliderValueSkinCr.Name = "sliderValueSkinCr";
            this.sliderValueSkinCr.Size = new System.Drawing.Size(258, 25);
            this.sliderValueSkinCr.TabIndex = 7;
            // 
            // sliderValueSkinY
            // 
            this.sliderValueSkinY.AutoSize = true;
            this.sliderValueSkinY.Location = new System.Drawing.Point(70, 26);
            this.sliderValueSkinY.Name = "sliderValueSkinY";
            this.sliderValueSkinY.Size = new System.Drawing.Size(258, 25);
            this.sliderValueSkinY.TabIndex = 5;
            // 
            // sliderValuesCb
            // 
            this.sliderValuesCb.AutoSize = true;
            this.sliderValuesCb.Location = new System.Drawing.Point(70, 87);
            this.sliderValuesCb.Name = "sliderValuesCb";
            this.sliderValuesCb.Size = new System.Drawing.Size(258, 25);
            this.sliderValuesCb.TabIndex = 7;
            // 
            // sliderValuesCr
            // 
            this.sliderValuesCr.AutoSize = true;
            this.sliderValuesCr.Location = new System.Drawing.Point(70, 57);
            this.sliderValuesCr.Name = "sliderValuesCr";
            this.sliderValuesCr.Size = new System.Drawing.Size(258, 25);
            this.sliderValuesCr.TabIndex = 7;
            // 
            // sliderValuesY
            // 
            this.sliderValuesY.AutoSize = true;
            this.sliderValuesY.Location = new System.Drawing.Point(70, 26);
            this.sliderValuesY.Name = "sliderValuesY";
            this.sliderValuesY.Size = new System.Drawing.Size(258, 25);
            this.sliderValuesY.TabIndex = 5;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Black;
            this.pictureBox2.BackgroundImage = global::HandGestureDetector.Properties.Resources.closed_76;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 76);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Black;
            this.pictureBox5.BackgroundImage = global::HandGestureDetector.Properties.Resources.pointing_76;
            this.pictureBox5.Location = new System.Drawing.Point(3, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(76, 76);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Black;
            this.pictureBox7.BackgroundImage = global::HandGestureDetector.Properties.Resources.OK_76;
            this.pictureBox7.Location = new System.Drawing.Point(3, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(76, 76);
            this.pictureBox7.TabIndex = 4;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Black;
            this.pictureBox8.BackgroundImage = global::HandGestureDetector.Properties.Resources.O_76;
            this.pictureBox8.Location = new System.Drawing.Point(3, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(76, 76);
            this.pictureBox8.TabIndex = 4;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Black;
            this.pictureBox6.BackgroundImage = global::HandGestureDetector.Properties.Resources.claw_76;
            this.pictureBox6.Location = new System.Drawing.Point(3, 3);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(76, 76);
            this.pictureBox6.TabIndex = 4;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.BackgroundImage = global::HandGestureDetector.Properties.Resources.open_hand_76;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 76);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Black;
            this.pictureBox3.Location = new System.Drawing.Point(3, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(76, 76);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Black;
            this.pictureBox4.Location = new System.Drawing.Point(3, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(76, 76);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(455, 482);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tabControl1);
            this.Name = "OptionsForm";
            this.Text = "Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabCamera.ResumeLayout(false);
            this.groupBoxSkin.ResumeLayout(false);
            this.groupBoxSkin.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabDetection.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_medium_distance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_min_area)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_cooldown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_failure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_success)).EndInit();
            this.tabOutput.ResumeLayout(false);
            this.tabOutput.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCamera;
        private System.Windows.Forms.TabPage tabDetection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabOutput;
        private System.Windows.Forms.GroupBox groupBox1;
        private SelectionRangeSliderValues sliderValuesY;
        private SelectionRangeSliderValues sliderValuesCb;
        private System.Windows.Forms.Label label4;
        private SelectionRangeSliderValues sliderValuesCr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxSkin;
        private SelectionRangeSliderValues sliderValueSkinCb;
        private System.Windows.Forms.Label label5;
        private SelectionRangeSliderValues sliderValueSkinCr;
        private System.Windows.Forms.Label label6;
        private SelectionRangeSliderValues sliderValueSkinY;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_upload_open;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_clear_script;
        private System.Windows.Forms.Button button_create_script;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numeric_success;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label_script_status;
        private System.Windows.Forms.NumericUpDown numeric_cooldown;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numeric_failure;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numeric_medium_distance;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numeric_min_area;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
    }
}